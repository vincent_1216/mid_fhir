function createOrganization() {
    var orgName = document.getElementById('orgName').value;
    var data = {
        resourceType: "Organization",
        name: orgName
    };

    fetch('https://hapi.fhir.org/baseR4/Organization', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/fhir+json'
        },
        body: JSON.stringify(data)
    })
    .then(response => response.json())
    .then(data => {
        alert('Organization created successfully. ID: ' + data.id);
        console.log('Organization ID:', data.id);
    })
    .catch((error) => {
        console.error('Error:', error);
        alert('Error creating organization');
    });
}

function createPractitioner() {
    var pracName = document.getElementById('pracName').value;
    var data = {
        resourceType: "Practitioner",
        name: [{
            text: pracName
        }]
    };

    fetch('https://hapi.fhir.org/baseR4/Practitioner', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/fhir+json'
        },
        body: JSON.stringify(data)
    })
    .then(response => response.json())
    .then(data => {
        alert('Practitioner created successfully. ID: ' + data.id);
        console.log('Practitioner ID:', data.id);
    })
    .catch((error) => {
        console.error('Error:', error);
        alert('Error creating practitioner');
    });
}
function assignPractitionerToOrganization() {
    var orgId = document.getElementById('orgId').value;
    var pracId = document.getElementById('pracId').value;
    var data = {
        resourceType: "PractitionerRole",
        practitioner: {
            reference: "Practitioner/" + pracId
        },
        organization: {
            reference: "Organization/" + orgId
        }
    };

    fetch('https://hapi.fhir.org/baseR4/PractitionerRole', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/fhir+json'
        },
        body: JSON.stringify(data)
    })
    .then(response => response.json())
    .then(data => {
        alert('Practitioner assigned to organization successfully');
        console.log(data);
    })
    .catch((error) => {
        console.error('Error:', error);
        alert('Error assigning practitioner to organization');
    });
}
