async function searchData() {
    var name = document.getElementById('searchName').value;
    var response = await fetch(`https://hapi.fhir.org/baseR4/Patient?name=${name}&organization=11450109`);
    var data = await response.json();
    let lastData = data.entry.length;

    console.log(data);
    if (data.total > 0) {
        var patient = data.entry[0].resource;
        var displayText = "姓名: " + patient.name[0].text + "<br>" +
                          "出生日期: " + patient.birthDate + "<br>";
        document.getElementById('patientInfo').innerHTML = displayText;
         
    } else {
        document.getElementById('patientInfo').innerHTML = "未找到患者信息";
    }
    let patientId = data.entry[lastData-1].resource.id;
    console.log(patientId);
    var response = await fetch(`https://hapi.fhir.org/baseR4/Observation?subject=${patientId}`);
    var data = await response.json();
    console.log(data);
    let observation = data.entry[0].resource.valueString;
    console.log(observation);
    document.getElementById('observation').innerHTML = observation;

    var response = await fetch(`https://hapi.fhir.org/baseR4/Procedure?subject=${patientId}`);
    var data = await response.json();
    console.log(data);
    let MedicalRecordsDisplayText = data.entry[lastData-1].resource.code.text;
    document.getElementById('MedicalRecords').innerHTML = MedicalRecordsDisplayText;
    console.log(MedicalRecordsDisplayText);

}