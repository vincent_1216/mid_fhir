async function create(){
    var patientName = document.getElementById('patientName').value;
    var patientDate = document.getElementById('birthDate').value;
    var patientGender = document.getElementById('gender').value;
    var symptomDescription = document.getElementById('symptomDescription').value;
    var treatmentRecord = document.getElementById('treatmentRecord').value;
    var patientResource = {
        resourceType: "Patient",
        name: [{
            use: "official", text: patientName
        }], "gender": patientGender,
        "birthDate": patientDate,
        managingOrganization: {
            reference: "Organization/11450109"
        },
    };

    let patientId
    await fetch('https://hapi.fhir.org/baseR4/Patient', {
       method: 'POST',
       headers: {
           'Content-Type': 'application/fhir+json'
       },
       body: JSON.stringify(patientResource)
   }).then(response => response.json())
       .then(data => {console.log('Observation 上傳成功:', data);
   if (data && data.id) {
        patientId = data.id;
       console.log('Patient ID:', patientId);
   }})

    var response = await fetch(`https://hapi.fhir.org/baseR4/Patient/42156772`);
    var data = await response.json();
    document.getElementById('displayArea').value +="patient:\n" + JSON.stringify(data);
    console.log(data);
}