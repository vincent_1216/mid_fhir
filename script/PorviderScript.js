async function submitData() {
    var patientName = document.getElementById('patientName').value;
    var symptomDescription = document.getElementById('symptomDescription').value;
    var treatmentRecord = document.getElementById('treatmentRecord').value;
/*--------------------------------patient------------------------------------*/ 
    var patientResource = {
        resourceType: "Patient",
        name: [{
            use: "official", text: patientName
        }], "gender": "male",
        "birthDate": "2000-12-16",
        managingOrganization: {
            reference: "Organization/11450109"
        },
    };
    let patientId
     await fetch('https://hapi.fhir.org/baseR4/Patient', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/fhir+json'
        },
        body: JSON.stringify(patientResource)
    }).then(response => response.json())
        .then(data => {console.log('Observation 上傳成功:', data);
    if (data && data.id) {
         patientId = data.id;
        console.log('Patient ID:', patientId);
    }})
/*--------------------------------patient------------------------------------*/ 
/*-------------------------------observation---------------------------------*/ 
  
    var observationResource = {
        resourceType: "Observation",
        status: "final",
        code: {
            coding: [{
                system: "http://loinc.org",
                code: "75325-1",
                display: "ALS Functional Rating Scale-Revised (ALSFRS-R)"
            }]
        },
        subject: {
            reference: "Patient/" + patientId
        },
        valueString: symptomDescription
    };

    await fetch('https://hapi.fhir.org/baseR4/Observation', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/fhir+json'
        },
        body: JSON.stringify(observationResource)
    }).then(response => response.json())
        .then(data => console.log('Observation 上傳成功:', data))
        .catch(error => console.error('Observation 上傳錯誤:', error));
/*--------------------------------observation---------------------------------*/ 
/*---------------------------------procedure---------------------------------*/ 

    var procedureResource = {
        resourceType: "Procedure",
        subject: {
            reference: "Patient/" + patientId
        },
        status: "completed",
        code: {
            text: treatmentRecord
        }
    };

    await fetch('https://hapi.fhir.org/baseR4/Procedure', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/fhir+json'
        },
        body: JSON.stringify(procedureResource)
    }).then(response => response.json())
        .then(data => console.log('Procedure 上傳成功:', data))
        .catch(error => console.error('Procedure 上傳錯誤:', error));
/*---------------------------------procedure---------------------------------*/ 
/*---------------------------------display---------------------------------*/ 
        var response = await fetch(`https://hapi.fhir.org/baseR4/Patient/${patientId}`);
        var data = await response.json();
        document.getElementById('displayArea').value +="patient:\n" + JSON.stringify(data);
        console.log(data);
        
        var response = await fetch(`https://hapi.fhir.org/baseR4/Observation?subject=${patientId}`);
        var data = await response.json();
        console.log(data);
        document.getElementById('displayArea').value +="\n\nobservation:\n" + JSON.stringify(data);
    
        var response = await fetch(`https://hapi.fhir.org/baseR4/Procedure?subject=${patientId}`);
        var data = await response.json();
        console.log(data);
        document.getElementById('displayArea').value += "\n\nMedicalRecords:\n" +JSON.stringify(data);
        console.log(data);
/*---------------------------------display---------------------------------*/ 
};
async function getData(){
    var response = await fetch(`https://hapi.fhir.org/baseR4/Patient/42156772`);
    var data = await response.json();
    document.getElementById('displayArea').value +="patient:\n" + JSON.stringify(data);
    console.log(data);
    
    var response = await fetch(`https://hapi.fhir.org/baseR4/Observation?subject=42156772`);
    var data = await response.json();
    console.log(data);
    document.getElementById('displayArea').value +="\n\nobservation:\n" + JSON.stringify(data);

    var response = await fetch(`https://hapi.fhir.org/baseR4/Procedure?subject=42156772}`);
    var data = await response.json();
    console.log(data);
    document.getElementById('displayArea').value += "\n\nMedicalRecords:\n" +JSON.stringify(data);
    console.log(data);

}